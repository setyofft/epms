<div id="preloader"></div>
    <!-- Preloader End Here -->
    <div id="wrapper" class="wrapper">
        <!-- Add your site or application content here -->
        <!-- Header Area Start Here -->

        <div class="container">
            <div class="navbar-baru">
                <nav class="navbar navbar-light bg-dark fixed-top">
                    <a class="navbar-brand" href="#">
                        <img src="<?=base_url()?>assets/img/logo.png" width="30" height="30" alt="">
                    </a>
                </nav>
            </div>
        </div>

        <!-- Header Area End Here -->
        <!-- Inne Page Banner Area Start Here -->
        <section class="inner-page-banner bg-common">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumbs-area">
                            <h1>Category Blogs</h1>
                            <ul>
                                <li>
                                    <a href="index-1.html">Home</a>
                                </li>
                                <li>Categories</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Inne Page Banner Area End Here -->
        <!-- Blog Area Start Here -->
        <section class="blog-wrap-layout20">
            <div class="container">
                <div class="row gutters-50">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-xl-12 col-lg-6 col-md-6 col-12">
                                <div class="row no-gutters">
                                    <div class="col-xl-6 col-lg-12">
                                        <div class="blog-box-layout10">
                                            <div class="item-img">
                                                <img src="<?=base_url()?>assets/img/blog/blog79.jpeg" alt="blog">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-12 d-flex align-items-center">
                                        <div class="blog-box-layout10">
                                            <div class="item-content">
                                                <ul class="entry-meta meta-color-dark">
                                                    <li><i class="fas fa-tag"></i>Flower</li>
                                                    <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                                    <li><i class="far fa-clock"></i>5 Mins Read</li>
                                                </ul>
                                                <h3 class="item-title"> <a href="single-blog.html">10 Bridal Bouquets You’ll
                                                        Fall Inner Love With</a></h3>
                                                <p>Phasellus lorem ligula, semper vehicula dolor vitae eleifen deary mattis
                                                    sem.
                                                    In hacBloggin GuideSed ut perspiciatis.</p>
                                                <ul class="response-area">
                                                    <li><a href="#"><i class="far fa-heart"></i>15</a></li>
                                                    <li><a href="#"><i class="far fa-comment"></i>02</a></li>
                                                </ul>
                                                <a href="single-blog.html" class="item-btn">READ MORE<i class="fas fa-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-6 col-md-6 col-12">
                                <div class="row no-gutters flex-lg-row-reverse">
                                    <div class="col-xl-6 col-lg-12">
                                        <div class="blog-box-layout10">
                                            <div class="item-img">
                                                <img src="<?=base_url()?>assets/img/blog/blog80.jpeg" alt="blog">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-12 d-flex align-items-center">
                                        <div class="blog-box-layout10">
                                            <div class="item-content">
                                                <ul class="entry-meta meta-color-dark">
                                                    <li><i class="fas fa-tag"></i>Weeding</li>
                                                    <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                                    <li><i class="far fa-clock"></i>5 Mins Read</li>
                                                </ul>
                                                <h3 class="item-title"> <a href="single-blog.html">Finding Your Own Styling a
                                                        City of Trendsetters</a></h3>
                                                <p>Phasellus lorem ligula, semper vehicula dolor vitae eleifen deary mattis
                                                    sem.
                                                    In hacBloggin GuideSed ut perspiciatis.</p>
                                                <ul class="response-area">
                                                    <li><a href="#"><i class="far fa-heart"></i>15</a></li>
                                                    <li><a href="#"><i class="far fa-comment"></i>02</a></li>
                                                </ul>
                                                <a href="single-blog.html" class="item-btn">READ MORE<i class="fas fa-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-6 col-md-6 col-12">
                                <div class="row no-gutters">
                                    <div class="col-xl-6 col-lg-12">
                                        <div class="blog-box-layout10">
                                            <div class="item-img">
                                                <img src="<?=base_url()?>assets/img/blog/blog81.jpeg" alt="blog">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-12 d-flex align-items-center">
                                        <div class="blog-box-layout10">
                                            <div class="item-content">
                                                <ul class="entry-meta meta-color-dark">
                                                    <li><i class="fas fa-tag"></i>Coupled</li>
                                                    <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                                    <li><i class="far fa-clock"></i>5 Mins Read</li>
                                                </ul>
                                                <h3 class="item-title"> <a href="single-blog.html">10 Bridal Bouquets You’ll
                                                        Fall Inner Love With</a></h3>
                                                <p>Phasellus lorem ligula, semper vehicula dolor vitae eleifen deary mattis
                                                    sem.
                                                    In hacBloggin GuideSed ut perspiciatis.</p>
                                                <ul class="response-area">
                                                    <li><a href="#"><i class="far fa-heart"></i>15</a></li>
                                                    <li><a href="#"><i class="far fa-comment"></i>02</a></li>
                                                </ul>
                                                <a href="single-blog.html" class="item-btn">READ MORE<i class="fas fa-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-6 col-md-6 col-12">
                                <div class="row no-gutters flex-lg-row-reverse">
                                    <div class="col-xl-6 col-lg-12">
                                        <div class="blog-box-layout10">
                                            <div class="item-img">
                                                <img src="<?=base_url()?>assets/img/blog/blog82.jpeg" alt="blog">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-12 d-flex align-items-center">
                                        <div class="blog-box-layout10">
                                            <div class="item-content">
                                                <ul class="entry-meta meta-color-dark">
                                                    <li><i class="fas fa-tag"></i>Dress</li>
                                                    <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                                    <li><i class="far fa-clock"></i>5 Mins Read</li>
                                                </ul>
                                                <h3 class="item-title"> <a href="single-blog.html">Finding Your Own Styling a
                                                        City of Trendsetters</a></h3>
                                                <p>Phasellus lorem ligula, semper vehicula dolor vitae eleifen deary mattis
                                                    sem.
                                                    In hacBloggin GuideSed ut perspiciatis.</p>
                                                <ul class="response-area">
                                                    <li><a href="#"><i class="far fa-heart"></i>15</a></li>
                                                    <li><a href="#"><i class="far fa-comment"></i>02</a></li>
                                                </ul>
                                                <a href="single-blog.html" class="item-btn">READ MORE<i class="fas fa-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-6 col-md-6 col-12">
                                <div class="row no-gutters">
                                    <div class="col-xl-6 col-lg-12">
                                        <div class="blog-box-layout10">
                                            <div class="item-img">
                                                <img src="<?=base_url()?>assets/img/blog/blog83.jpeg" alt="blog">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-12 d-flex align-items-center">
                                        <div class="blog-box-layout10">
                                            <div class="item-content">
                                                <ul class="entry-meta meta-color-dark">
                                                    <li><i class="fas fa-tag"></i>Dress</li>
                                                    <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                                    <li><i class="far fa-clock"></i>5 Mins Read</li>
                                                </ul>
                                                <h3 class="item-title"> <a href="single-blog.html">10 Bridal Bouquets You’ll
                                                        Fall Inner Love With</a></h3>
                                                <p>Phasellus lorem ligula, semper vehicula dolor vitae eleifen deary mattis
                                                    sem.
                                                    In hacBloggin GuideSed ut perspiciatis.</p>
                                                <ul class="response-area">
                                                    <li><a href="#"><i class="far fa-heart"></i>15</a></li>
                                                    <li><a href="#"><i class="far fa-comment"></i>02</a></li>
                                                </ul>
                                                <a href="single-blog.html" class="item-btn">READ MORE<i class="fas fa-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-6 col-md-6 col-12">
                                <div class="row no-gutters flex-lg-row-reverse">
                                    <div class="col-xl-6 col-lg-12">
                                        <div class="blog-box-layout10">
                                            <div class="item-img">
                                                <img src="<?=base_url()?>assets/img/blog/blog84.jpeg" alt="blog">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-12 d-flex align-items-center">
                                        <div class="blog-box-layout10">
                                            <div class="item-content">
                                                <ul class="entry-meta meta-color-dark">
                                                    <li><i class="fas fa-tag"></i>Dress</li>
                                                    <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                                    <li><i class="far fa-clock"></i>5 Mins Read</li>
                                                </ul>
                                                <h3 class="item-title"> <a href="single-blog.html">10 Bridal Bouquets You’ll
                                                        Fall Inner Love With</a></h3>
                                                <p>Phasellus lorem ligula, semper vehicula dolor vitae eleifen deary mattis
                                                    sem.
                                                    In hacBloggin GuideSed ut perspiciatis.</p>
                                                <ul class="response-area">
                                                    <li><a href="#"><i class="far fa-heart"></i>15</a></li>
                                                    <li><a href="#"><i class="far fa-comment"></i>02</a></li>
                                                </ul>
                                                <a href="single-blog.html" class="item-btn">READ MORE<i class="fas fa-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>                            
                                </div>
                            </div>
                        </div>
                        <div class="pagination-layout1 pt--20">
                            <ul>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                            </ul>
                        </div>                        
                    </div>
                    <div class="col-lg-4 sidebar-widget-area sidebar-break-md">
                        <div class="widget">
                            <div class="section-heading heading-dark">
                                <h3 class="item-heading">ABOUT ME</h3>
                            </div>
                            <div class="widget-about-3">
                                <div class="item-img">
                                    <img src="<?=base_url()?>assets/img/about/about1.jpeg" alt="Brand" class="img-fluid">
                                </div>
                                <div class="item-content">
                                    <span class="item-sign"><img src="<?=base_url()?>assets/img/figure/signature.png" alt="sign"></span>
                                    <p>Fusce mauris auctor ollicituderty
                                        iner hendrerit risus aeenean
                                        rauctor pibus doloer.</p>
                                </div>
                            </div>
                        </div>
                        <div class="widget">
                            <div class="section-heading heading-dark">
                                <h3 class="item-heading">FOLLOW ME ON</h3>
                            </div>
                            <div class="widget-follow-us-2">
                                <ul>
                                    <li class="single-item"><a href="#"><i class="fab fa-facebook-f"></i>LIKE ME ON</a></li>
                                    <li class="single-item"><a href="#"><i class="fab fa-twitter"></i>52K FOLLOWERS</a></li>
                                    <li class="single-item"><a href="#"><i class="fab fa-instagram"></i>FOLLOW ME</a></li>
                                    <li class="single-item"><a href="#"><i class="fab fa-linkedin-in"></i>FOLLOW ME</a></li>
                                    <li class="single-item"><a href="#"><i class="fab fa-pinterest-p"></i>FOLLOW ME</a></li>
                                    <li class="single-item"><a href="#"><i class="fab fa-youtube"></i>SUBSCRIBE</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget">
                            <div class="widget-newsletter-subscribe-dark">
                                <h3>GET LATEST UPDATES</h3>
                                <p>NEWSLETTER SUBSCRIBE</p>
                                <form class="newsletter-subscribe-form">
                                    <div class="form-group">
                                        <input type="text" placeholder="your e-mail address" class="form-control" name="email" data-error="E-mail field is required" required="">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group mb-none">
                                        <button type="submit" class="item-btn">SUBSCRIBE</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="widget">
                            <div class="section-heading heading-dark">
                                <h3 class="item-heading">LATEST POSTS</h3>
                            </div>
                            <div class="widget-latest">
                                <ul class="block-list">
                                    <li class="single-item">
                                        <div class="item-img">
                                            <a href="#"><img src="<?=base_url()?>assets/img/blog/blog85.jpeg" alt="Post"></a>
                                        </div>
                                        <div class="item-content">
                                            <ul class="entry-meta meta-color-dark">
                                                <li><i class="fas fa-tag"></i>Weeding</li>
                                                <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                            </ul>
                                            <h4 class="item-title"><a href="#">Thought aful Living result are aert aos
                                                    Angeles</a></h4>
                                        </div>
                                    </li>
                                    <li class="single-item">
                                        <div class="item-img">
                                            <a href="#"><img src="<?=base_url()?>assets/img/blog/blog86.jpeg" alt="Post"></a>
                                        </div>
                                        <div class="item-content">
                                            <ul class="entry-meta meta-color-dark">
                                                <li><i class="fas fa-tag"></i>Flower</li>
                                                <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                            </ul>
                                            <h4 class="item-title"><a href="#">Type designer Jeremy Tanka rdoverhauls
                                                    online</a></h4>
                                        </div>
                                    </li>
                                    <li class="single-item">
                                        <div class="item-img">
                                            <a href="#"><img src="<?=base_url()?>assets/img/blog/blog87.jpeg" alt="Post"></a>
                                        </div>
                                        <div class="item-content">
                                            <ul class="entry-meta meta-color-dark">
                                                <li><i class="fas fa-tag"></i>Stage</li>
                                                <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                            </ul>
                                            <h4 class="item-title"><a href="#">5 design things to look out for in June
                                                    2019</a></h4>
                                        </div>
                                    </li>
                                    <li class="single-item">
                                        <div class="item-img">
                                            <a href="#"><img src="<?=base_url()?>assets/img/blog/blog88.jpeg" alt="Post"></a>
                                        </div>
                                        <div class="item-content">
                                            <ul class="entry-meta meta-color-dark">
                                                <li><i class="fas fa-tag"></i>Life Style</li>
                                                <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                            </ul>
                                            <h4 class="item-title"><a href="#">Marc Falzone opens £2 million UK Expo
                                                    Pavilion</a></h4>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget">
                            <div class="section-heading heading-dark">
                                <h3 class="item-heading">INSTAGRAM</h3>
                            </div>
                            <div class="widget-instagram">
                                <ul>
                                    <li>
                                        <div class="item-box">
                                            <img src="<?=base_url()?>assets/img/social-figure/social-figure47.jpeg" alt="Social Figure" class="img-fluid">
                                            <a href="#" class="item-icon"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-box">
                                            <img src="<?=base_url()?>assets/img/social-figure/social-figure48.jpeg" alt="Social Figure" class="img-fluid">
                                            <a href="#" class="item-icon"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-box">
                                            <img src="<?=base_url()?>assets/img/social-figure/social-figure49.jpeg" alt="Social Figure" class="img-fluid">
                                            <a href="#" class="item-icon"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-box">
                                            <img src="<?=base_url()?>assets/img/social-figure/social-figure50.jpeg" alt="Social Figure" class="img-fluid">
                                            <a href="#" class="item-icon"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-box">
                                            <img src="<?=base_url()?>assets/img/social-figure/social-figure51.jpeg" alt="Social Figure" class="img-fluid">
                                            <a href="#" class="item-icon"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-box">
                                            <img src="<?=base_url()?>assets/img/social-figure/social-figure52.jpeg" alt="Social Figure" class="img-fluid">
                                            <a href="#" class="item-icon"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-box">
                                            <img src="<?=base_url()?>assets/img/social-figure/social-figure53.jpeg" alt="Social Figure" class="img-fluid">
                                            <a href="#" class="item-icon"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-box">
                                            <img src="<?=base_url()?>assets/img/social-figure/social-figure54.jpeg" alt="Social Figure" class="img-fluid">
                                            <a href="#" class="item-icon"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-box">
                                            <img src="<?=base_url()?>assets/img/social-figure/social-figure55.jpeg" alt="Social Figure" class="img-fluid">
                                            <a href="#" class="item-icon"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Blog Area End Here -->
        <!-- Footer Area Start Here -->
        <footer class="footer-wrap-layout2">
            <div class="container">
                <div class="footer-box-layout2">
                    <div class="footer-logo">
                        <a href="index-1.html"><img src="<?=base_url()?>assets/img/logo2.png" width="150px" alt="logo"></a>
                    </div>
                    <ul class="footer-social">
                        <li><a href="#"><i class="fab fa-facebook-f"></i>259k LIKES</a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i>480k FOLLOWERS</a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i>280k FOLLOWERS</a></li>
                        <li><a href="#"><i class="fab fa-youtube"></i>180k SUBSCRIBER</a></li>
                    </ul>
                    <div class="copyright">© 2019 blogxer. All Rights Reserved.</div>
                </div>
            </div>
        </footer>
        <!-- Footer Area End Here -->
        <!-- Search Box Start Here -->
        <div id="header-search" class="header-search">
            <button type="button" class="close">×</button>
            <form class="header-search-form">
                <input type="search" value="" placeholder="Type here........">
                <button type="submit" class="search-btn">
                    <i class="flaticon-magnifying-glass"></i>
                </button>
            </form>
        </div>
        <!-- Search Box End Here -->
    </div>