<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

    public function __construct(){
        parent::__construct();
    }
    public function index(){
        $this->load->view('template/header');
        $this->load->view('dashboard');
        $this->load->view('template/js');
        $this->load->view('js-dashbaord');

    }
    public function detail(){
        $this->load->view('template/header');
        $this->load->view('detail');
        $this->load->view('template/js');

    }

}