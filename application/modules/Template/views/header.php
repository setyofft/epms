<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>e-PMS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url()?>assets/img/favicon3.png" width="100%";>

    <link rel="stylesheet" href="<?=base_url()?>assets/css/normalize.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/css/main.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/css/fontawesome-all.min.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/font/flaticon.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/css/animate.min.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/font.css">

    <script src="<?=base_url()?>assets/js/modernizr-3.6.0.min.js"></script>
</head>

<body class="sticky-header">