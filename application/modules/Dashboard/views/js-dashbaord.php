<script>
    $('.ntgl').css('display','none');
    $('.tntgl').css('display','none');
    $('.btn-hide').css('display','none');

    $('#kategori').on("change",function(){
        $('#hasil_hitung').html('');
        $('.btn-hide').css('display','none');
        var kondisi = $(this).val();
        if(kondisi==1){
            $('.ntgl').css('display','block');
            $('.tntgl').css('display','none');
        }else if(kondisi==0){
            $('.tntgl').css('display','block');
            $('.ntgl').css('display','none');
        }
    })

    $('#tgl-normal').on("change",function(){
        $('.btn-hide').css('display','block');
    })

    $('#tgl-M-terakhir').on("change",function(){
        $('.btn-hide').css('display','block');
    })

    $('#hitung-masa').click(function(){
        var kategori = $('#kategori').val();
        var tgln = $('#tgl-normal').val();
        var Spen = $('#siklus-pen').val();
        var Span = $('#siklus-pan').val();
        var tgl_Amens = $('#tgl-M-terakhir').val();

        // .toISOString().slice(0,10);
        if(kategori==1){
             // normal siklus
            var hariKedepan = new Date(new Date(tgln).getTime()+(28*24*60*60*1000));
            var rentang = new Date(new Date(hariKedepan).getTime()-(14*24*60*60*1000));
            var satulbh = new Date(new Date(rentang).getTime()+(1*24*60*60*1000));
            var satukrg = new Date(new Date(rentang).getTime()-(1*24*60*60*1000));

            var hsl_htng = '<p>tgl mens berikutnya:'+hariKedepan.toISOString().slice(0,10)+'</p><br/>'+'<p>masa subur tgl:'+rentang.toISOString().slice(0,10)+'<br/>'+satulbh.toISOString().slice(0,10)+'<br/>'+satukrg.toISOString().slice(0,10)+'</p>';
            $('#hasil_hitung').html(hsl_htng);
            // end normal sklus
        }else if(kategori==0){
            // tidak normal / tdk teratur
            if(Spen<=21 || Span>=35){
                alert('Tidak Bisa Dihitung');
            }else{
                var HSpen = Spen-18;
                var HSpan = Span-11;
                var Msubur = new Date(new Date(tgl_Amens).getTime()+(HSpen*24*60*60*1000));
                var Hsubur = new Date(new Date(Msubur).getTime()+(HSpan*24*60*60*1000));

                var hsl_htng1 = '<p>Berlangsung:'+HSpan+' hari</p><p>tgl Subur Mulai:'+Msubur.toISOString().slice(0,10)+'</p><br/>'+'<p>Samapai tgl:'+Hsubur.toISOString().slice(0,10);
                $('#hasil_hitung').html(hsl_htng1);
            }
            //end tidak normal / tdk teratur
        }

    })
</script>