<a href="#wrapper" data-type="section-switch" class="scrollup">
        <i class="fas fa-angle-double-up"></i>
</a>

<div id="preloader"></div>

<div id="wrapper" class="wrapper" data-bg-image="<?=base_url()?>assets/img/figure/home-bg.png">
        <!-- Add your site or application content here -->
        <!-- Header Area Start Here -->
        <header>
            <div id="rt-sticky-placeholder"></div>
            <div id="header-menu" class="menu-layout4 bg--light">
                <div class="header-menu-content">
                    <div class="header-logo">
                        <a href="index-1.html"><img src="<?=base_url()?>assets/img/logo.png"  width="100px"></a>
                    </div>
                    <div class="offcanvas-menu-trigger-wrap">
                        <button type="button" class="offcanvas-menu-btn menu-status-open">
                            <span class="btn-icon-wrap">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header Area Start Here -->
        <!-- Offcanvas Area Start Here -->
        <div class="offcanvas-menu-wrap open" id="offcanvas-wrap">
            <div class="offcanvas-close close-btn">x</div>
            <div class="offcanvas-content">
                <div class="offcanvas-logo">
                    <a href="index-1.html"><img src="<?=base_url()?>assets/img/logo.png" alt="logo"></a>
                </div>


                <div class="box-cricle">
                    <div class="item-title m-t-10 text-center">Konsultasi Dokter</div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <a href="">
                            <figure class="img-cricle"><img src="./assets/img/figure/figure.jpeg" alt="about"></figure></a>
                            <img src="./assets/img/on.png" class="status-icon">
                            <h5 class="nm-dokter">Naomi</h5>
                        </div>
                        <div class="col-md-4 text-center">
                            <a href="">
                            <figure class="img-cricle"><img src="./assets/img/figure/figure.jpeg" alt="about"></figure></a>
                            <img src="./assets/img/on.png" class="status-icon">
                            <h5 class="nm-dokter">Bambang</h5>
                        </div>
                        <div class="col-md-4 text-center">
                            <a href="">
                            <figure class="img-cricle"><img src="./assets/img/figure/figure.jpeg" alt="about"></figure></a>
                            <img src="./assets/img/off.png" class="status-icon">
                            <h5 class="nm-dokter">Anastasya</h5>
                        </div>

                        <div class="col-md-4 text-center">
                            <a href="">
                            <figure class="img-cricle"><img src="./assets/img/figure/figure.jpeg" alt="about"></figure></a>
                            <img src="./assets/img/off.png" class="status-icon">
                            <h5 class="nm-dokter">Adit</h5>
                        </div>
                    </div>
                </div>

                <div class="offcanvas-footer">
                    <div class="item-title">Subcribe & Follow</div>
                    <ul class="offcanvas-social">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>

                    </ul>
                </div>
            </div>
        </div>
        <!-- Offcanvas Area End Here -->
        <div class="page-content-wrap">
            <div class="row">
                <div class="col-xl-6 col-12" id="fixed-bar-coloum">
                    <div class="about-box-layout1">
                        <div class="item-img">
                            <img src="<?=base_url()?>assets/img/about/about.jpeg" alt="about">
                            <div class="item-content">

                                <!-- awal form -->
                                <div class="widget">
                                    <div class="widget-newsletter-subscribe-dark">
                                        <h3 style="font-family: 'Miss';font-size:45pt">Sistem</h3><br>
                                        <h3 style="font-family: 'Miss';font-size:30pt">Perhitungan</h3>
                                        <h3 style="font-family: 'Miss';font-size:20pt">Masa Subur</h3>

                                        <div class="form-group">
                                            <select name="kategori" id="kategori" class="form-control input-modif">
                                                <option class="option" value="1">Pilih Kondisi Menstruasi</option>
                                                <option class="option" value="1">Teratur / Normal</option>
                                                <option class="option" value="0">Tidak Teratur / Tidak Normal</option>
                                            </select>
                                        </div>

                                        <button type="button" class="item-btn btn-modif" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat">Open modal for @fat</button>
                                        <!-- normal -->
                                        <div class="form-group ntgl">
                                            <input type="date" class="form-control input-modif" id="tgl-normal" placeholder="Lama siklus menstruasi terpendek (ex.28)" required>
                                        </div>
                                        <!-- tidak normal -->
                                        <div class="tntgl">
                                            <div class="form-group tntgl">
                                                <input type="text" class="form-control input-modif" id="siklus-pen" placeholder="Siklus Terpendek Menstruasi" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control input-modif" id="siklus-pan" placeholder="Siklus Terpanjang Menstruasi" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="date" class="form-control input-modif" id="tgl-M-terakhir" placeholder="Tanggal Terakhir Menstruasi" required>
                                            </div>
                                        </div>

                                        <button type="submit" class="item-btn btn-modif btn-hide" id="hitung-masa">Hitung Masa Subur</button>
                                        <div id="hasil_hitung">

                                        </div>
                                    </div>
                                </div>
                                <!-- akhhir form -->

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-12">
                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <div class="blog-box-layout17">
                                <div class="item-img">
                                    <a href="<?php base_url()?>dashboard/detail"><img src="<?=base_url()?>assets/img/blog/blog5.jpeg" alt="Blog"></a>
                                </div>
                                <div class="item-content">
                                    <ul class="entry-meta meta-color-dark">
                                        <li><i class="fas fa-tag"></i>Kesehatan</li>
                                        <li><i class="fas fa-calendar-alt"></i>Agt 19, 2019</li>
                                    </ul>
                                    <h3 class="item-title"><a href="single-blog.html">Inovasi Alat Kesehatan, Indonesia Berhasil Ekspor Meja Operasi</a></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="blog-box-layout17">
                                <div class="item-img">
                                    <a href="single-blog.html"><img src="<?=base_url()?>assets/img/blog/blog6.jpeg" alt="Blog"></a>
                                </div>
                                <div class="item-content">
                                    <ul class="entry-meta meta-color-dark">
                                        <li><i class="fas fa-tag"></i>Artikel Beauty</li>
                                        <li><i class="fas fa-calendar-alt"></i>Agt 10, 2019</li>
                                    </ul>
                                    <h3 class="item-title"><a href="single-blog.html">8 Manfaat Kesehatan dari Buah Pomelo, Si Jeruk Bali</a></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="blog-box-layout17">
                                <div class="item-img">
                                    <a href="single-blog.html"><img src="<?=base_url()?>assets/img/blog/blog7.jpeg" alt="Blog"></a>
                                </div>
                                <div class="item-content">
                                    <ul class="entry-meta meta-color-dark">
                                        <li><i class="fas fa-tag"></i>Diet</li>
                                        <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                    </ul>
                                    <h3 class="item-title"><a href="single-blog.html">Fashion week summer  Collection 2018</a></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="blog-box-layout17">
                                <div class="item-img">
                                    <a href="single-blog.html"><img src="<?=base_url()?>assets/img/blog/blog8.jpeg" alt="Blog"></a>
                                </div>
                                <div class="item-content">
                                    <ul class="entry-meta meta-color-dark">
                                        <li><i class="fas fa-tag"></i>Design</li>
                                        <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                    </ul>
                                    <h3 class="item-title"><a href="single-blog.html">Fashion week summer  Collection 2018</a></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="blog-box-layout17">
                                <div class="item-img">
                                    <a href="single-blog.html"><img src="<?=base_url()?>assets/img/blog/blog9.jpeg" alt="Blog"></a>
                                </div>
                                <div class="item-content">
                                    <ul class="entry-meta meta-color-dark">
                                        <li><i class="fas fa-tag"></i>Design</li>
                                        <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                    </ul>
                                    <h3 class="item-title"><a href="single-blog.html">Fashion week summer  Collection 2018</a></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="blog-box-layout17">
                                <div class="item-img">
                                    <a href="single-blog.html"><img src="<?=base_url()?>assets/img/blog/blog10.jpeg" alt="Blog"></a>
                                </div>
                                <div class="item-content">
                                    <ul class="entry-meta meta-color-dark">
                                        <li><i class="fas fa-tag"></i>Design</li>
                                        <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                    </ul>
                                    <h3 class="item-title"><a href="single-blog.html">Fashion week summer  Collection 2018</a></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="blog-box-layout17">
                                <div class="item-img">
                                    <a href="single-blog.html"><img src="<?=base_url()?>assets/img/blog/blog11.jpeg" alt="Blog"></a>
                                </div>
                                <div class="item-content">
                                    <ul class="entry-meta meta-color-dark">
                                        <li><i class="fas fa-tag"></i>Design</li>
                                        <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                    </ul>
                                    <h3 class="item-title"><a href="single-blog.html">Fashion week summer  Collection 2018</a></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="blog-box-layout17">
                                <div class="item-img">
                                    <a href="single-blog.html"><img src="<?=base_url()?>assets/img/blog/blog5.jpeg" alt="Blog"></a>
                                </div>
                                <div class="item-content">
                                    <ul class="entry-meta meta-color-dark">
                                        <li><i class="fas fa-tag"></i>Design</li>
                                        <li><i class="fas fa-calendar-alt"></i>Jan 19, 2019</li>
                                    </ul>
                                    <h3 class="item-title"><a href="single-blog.html">Fashion week summer  Collection 2018</a></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-groupgi">
            <label for="recipient-name" class="col-form-label">Recipient:</label>
            <input type="text" class="form-control input-modif" id="recipient-name">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control input-modif" id="message-text"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="item-btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="item-btn btn-primary">Send message</button>
      </div>
    </div>
  </div>
</div>